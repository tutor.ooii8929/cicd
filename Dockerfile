FROM golang:alpine
ENV GO111MODULE=on
RUN         mkdir -p /app
WORKDIR     /app
COPY        . .
RUN go mod download
RUN go build -o ./out/dist .
CMD ./out/dist